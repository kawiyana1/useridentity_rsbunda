﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using UserIdentity.Entities;
using UserIdentity.Helper;

namespace UserIdentity.Controllers
{
    [Authorize(Roles = "UserIdentity")]
    public class RoleController : Controller
    {
        #region ===== R O L E M A N A G E R

        private ApplicationRoleManager _roleManager;

        public RoleController()
        {

        }

        public RoleController(ApplicationRoleManager roleManager)
        {
            RoleManager = roleManager;
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, Dictionary<string, object> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    IQueryable<AspNetRoles> proses = s.AspNetRoles;
                    foreach (var x in filter)
                    {
                        var y = (string[])x.Value;
                        if (!string.IsNullOrEmpty(y[1]))
                        {
                            if (y[0] == "contains") proses = proses.Where($"{x.Key}.Contains(@0)", y[1]);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Id,
                        Name = x.Name
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, string id, string name)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        if (_process == "CREATE")
                        {
                            var m = new IdentityRole(name);
                            var r = RoleManager.Create(m);
                            if (!r.Succeeded) throw new Exception(String.Join(", ", r.Errors));
                        }
                        else if (_process == "EDIT")
                        {
                            var m = s.AspNetRoles.FirstOrDefault(x => x.Id == id);
                            m.Name = name;
                        }
                        else if (_process == "DELETE")
                        {
                            var m = s.AspNetRoles.FirstOrDefault(x => x.Id == id);
                            if (m == null) throw new Exception("Data Tidak ditemukan");
                            name = m.Name;
                            s.AspNetRoles.Remove(m);
                        }

                        s.SaveChanges();
                        //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        //{
                        //    Activity = $"role-{_process}; id:{id}; nama:{name};".ToLower()
                        //};
                        //UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.AspNetRoles.FirstOrDefault(x => x.Id == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            RoleId = m.Id,
                            RoleName = m.Name
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}