﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using UserIdentity.Entities;
using UserIdentity.Helper;
using UserIdentity.Models;

namespace UserIdentity.Controllers
{
    [Authorize(Roles = "UserIdentity")]
    public class UserController : Controller
    {
        #region ===== U S E R M A N A G E R

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public UserController()
        {

        }

        public UserController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #endregion

        #region ===== L I S T

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, Dictionary<string, object> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    IQueryable<AspNetUsers> proses = s.AspNetUsers;
                    foreach (var x in filter)
                    {
                        var y = (string[])x.Value;
                        if (!string.IsNullOrEmpty(y[1]))
                        {
                            if (y[0] == "contains") proses = proses.Where($"{x.Key}.Contains(@0)", y[1]);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Id,
                        Email = x.Email,
                        Username = x.UserName
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, string id, string email, string username, string password, List<string> details)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        if (_process == "CREATE")
                        {
                            var user = new ApplicationUser
                            {
                                Email = email,
                                UserName = username
                            };
                            var r = UserManager.CreateAsync(user, string.IsNullOrEmpty(password) ? "X" : password);
                            if (!r.Result.Succeeded) throw new Exception(String.Join(", ", r.Result.Errors));
                        }
                        else if (_process == "EDIT")
                        {
                            var m = s.AspNetUsers.FirstOrDefault(x => x.Id == id);
                            m.Email = email;
                            m.UserName = username;

                            #region Detail
                            //var realdetail = s.VW_UserRole.Where(x => x.UserId == id).ToList();
                            var realdetail = s.AspNetUsers.Where(x => x.Id == id).SelectMany(x => x.AspNetRoles).ToList();

                            var newdetail = details;
                            // delete
                            foreach (var x in realdetail)
                            {
                                var n = newdetail.FirstOrDefault(y => y == x.Id);
                                if (n == null)
                                {
                                    s.Database.ExecuteSqlCommand(
                                        "DELETE FROM AspNetUserRoles WHERE RoleId=@roleid AND UserId = @userid",
                                        new SqlParameter("@userid", id),
                                        new SqlParameter("@roleid", x.Id)
                                    );
                                }
                            }
                            foreach (var x in newdetail)
                            {
                                var n = realdetail.FirstOrDefault(y => y.Id == x);
                                // add
                                if (n == null)
                                {
                                    s.Database.ExecuteSqlCommand(
                                        "INSERT INTO AspNetUserRoles VALUES (@userid, @roleid)",
                                        new SqlParameter("@userid", id),
                                        new SqlParameter("@roleid", x));
                                }
                                // edit
                                else
                                {

                                }
                            }

                            #endregion

                        }
                        else if (_process == "DELETE")
                        {
                            var m = s.AspNetUsers.FirstOrDefault(x => x.Id == id);
                            if (m == null) throw new Exception("Data Tidak ditemukan");
                            email = m.Email;

                            var d = s.Database.ExecuteSqlCommand(
                                "DELETE FROM AspNetUserRoles WHERE UserId = @userid",
                                new SqlParameter("@userid", id)
                            );

                            s.AspNetUsers.Remove(m);
                        }

                        s.SaveChanges();
                        //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        //{
                        //    Activity = $"role-{_process}; id:{id}; email:{email};".ToLower()
                        //};
                        //UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.AspNetUsers.FirstOrDefault(x => x.Id == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.AspNetUsers.Where(x => x.Id == id).SelectMany(x => x.AspNetRoles).ToList();

                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.Id,
                            UserName = m.UserName,
                            Email = m.Email
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            RoleId = x.Id,
                            RoleName = x.Name
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}