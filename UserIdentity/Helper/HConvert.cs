﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserIdentity.Helper
{
    public static class HConvert
    {
        public static string Error(Exception ex)
        {
            if (ex.InnerException != null) ex = ex.InnerException;
            if (ex.InnerException != null) ex = ex.InnerException;
            return JsonConvert.SerializeObject(new { IsSuccess = false, Message = ex.Message });
        }

        public static string Success()
        {
            return JsonConvert.SerializeObject(new { IsSuccess = true });
        }
    }
}