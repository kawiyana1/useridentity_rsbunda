﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserIdentity.Entities;

namespace UserIdentity.Helper
{
    public static class HList
    {
        public static List<SelectListItem> Roles
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.AspNetRoles.ToList();
                    return m.ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id
                    });
                }
            }
        }
    }
}