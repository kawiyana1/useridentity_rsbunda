﻿class iSetup {
    constructor(opt = {}) {
        this.modal = $("#" + opt.modal);
        this.form = this.modal.find('form')[0];
        this.urldetail = opt.urldetail;
        this.urlsave = opt.urlsave;
        this.saveparam = opt.saveparam;
        this.savesuccess = opt.savesuccess;
        this.detailsuccess = opt.detailsuccess;
        this.setupready = opt.setupready;

        this.loading = false;
        var t = this;

        $(this.form).on('submit', function (event) {
            event.preventDefault();
            if (t.loading) return;
            $.ajax({
                url: t.urlsave,
                type: 'POST',
                data: t.saveparam(),
                success: function (r) { t.savesuccess(r); },
                error: function (xhr) { alert(xhr.statusText); },
                beforeSend: function () { t.loading = true; loading(true); },
                complete: function () { t.loading = false; loading(false); }
            });
        });
    }

    opensetup(proc, param) {
        this.form['_PROCESS'].value = proc;
        if (proc === 'EDIT' || proc === 'CREATE') {
            this.modal.find('.btn-delete').addClass('hide');
            this.modal.find('.btn-save').removeClass('hide');
        } else {
            this.modal.find('.btn-save').addClass('hide');
            this.modal.find('.btn-delete').removeClass('hide');
        }
        if (proc === "CREATE") {
            this.form.reset();
            this.modal.modal();
        } else if (proc === "EDIT" || proc === "DELETE") {
            var t = this;
            $.ajax({
                url: this.urldetail,
                type: 'POST',
                data: param,
                success: function (r) { t.detailsuccess(r, proc); },
                error: function (xhr) { alert(xhr.statusText); },
                beforeSend: function () { loading(true); },
                complete: function () { loading(false); }
            });
        }
        this.setupready(proc, param);
    }
}

class iTable {
    constructor(opt = {}) {
        this.table = opt.table;
        this.property(opt);
        this.onloading = false;
        this.url = opt.url;
        this.tablesuccess = opt.tablesuccess;
        this.tablebeforeSend = opt.tablebeforeSend;
        var t = this;

        $(this.table).find('[data-itable-sorting]').on('click', function () {
            var _orderby = $(this).data('itable-sorting');
            var _orderbytype = _orderby === t.orderby ? !t.orderbytype : true;
            t.property({ orderby: _orderby, orderbytype: _orderbytype });
            t.refresh();
        });

        $(this.table).on('submit', function (event) {
            event.preventDefault();
            t.refresh();
        });
    }

    refresh() {
        if (this.onloading) return;
        this.pagesize = parseInt($(this.table).find('.itable-pagesize option:selected').val());
        var filter = [];
        $.each($(this.table).find('[data-itable-filter]'), function (index, value) {
            filter.push({
                Key: $(value).data('itable-filter'),
                Value: [
                    $(value).data('itable-filter-type') || 'contains',
                    value.type === 'checkbox' ? value.checked : value.value
                ]
            });
        });
        var t = this;
        $(this.table).find('tbody').html('');
        $.ajax({
            url: this.url,
            type: 'POST',
            data: {
                orderby: this.orderby,
                orderbytype: this.orderbytype,
                pagesize: this.pagesize,
                pageindex: this.pageindex,
                filter: filter
            },
            success: function (r) {
                r = JSON.parse(r);
                t.tablesuccess(r, t);
                if (r.IsSuccess) {
                    $(t.table).find('[data-itable-page]').on('click', function () {
                        t.property({ pageindex: $(this).data('itable-page') });
                        t.refresh();
                    });
                }
            },
            error: function (xhr) { alert(xhr.statusText); },
            beforeSend: function () { t.onloading = true; loading(true); if (t.tablebeforeSend === 'function') { t.tablebeforeSend(); } },
            complete: function () { t.onloading = false; loading(false); }
        });
    }

    property(opt = {}) {
        this.orderby = opt.orderby || this.orderby;
        var _orderbytype = this.orderbytype === undefined ? true : this.orderbytype;
        this.orderbytype = opt.orderbytype === undefined ? _orderbytype : opt.orderbytype;
        var _pageindex = this.pageindex === undefined ? 0 : this.pageindex;
        this.pageindex = opt.pageindex === undefined ? _pageindex : opt.pageindex;
        $(this.table).find('[data-itable-sorting] i').removeClass('glyphicon').removeClass('glyphicon-sort-by-attributes').removeClass('glyphicon-sort-by-attributes-alt');
        $(this.table).find('[data-itable-sorting="' + this.orderby + '"] i').addClass('glyphicon').addClass('glyphicon-sort-by-attributes' + (this.orderbytype ? '' : '-alt'));
    }
}

function generatefootertable(totalcount, pagesize, currentpage, functionname) {
    var startdata;
    if (totalcount === 0) startdata = 0;
    else startdata = currentpage * pagesize + 1;
    var maxpage = Math.ceil(totalcount / pagesize);
    var enddata = startdata + pagesize - 1;
    if (enddata > totalcount) enddata = totalcount;
    var datainfo = 'Data ' + startdata + '-' + enddata +
        ' of ' + totalcount +
        ' Page (' + (totalcount === 0 ? 0 : currentpage + 1) + '/' + maxpage + ')';

    var pagination = "";
    if (totalcount > 0) {
        var start_page = currentpage > 0 ? currentpage - 1 : 0;
        var end_page = currentpage + 1;
        if (currentpage === 0) end_page++;
        end_page = end_page > maxpage - 1 ? maxpage - 1 : end_page;
        if (currentpage >= end_page && start_page > 0) start_page--;
        var disable = ' class="disabled"';
        var string_page = '<li' + (currentpage === 0 ? disable : '') + '><a' + (currentpage === 0 ? '' : ' data-itable-page="0"') + '><span>&laquo;</span></a></li>';
        string_page += '<li' + (currentpage === 0 ? disable : '') + '><a' + (currentpage === 0 ? '' : ' data-itable-page="' + (currentpage - 1) + '"') + '><span>&lsaquo;</span></a></li>';
        if (start_page > 0)
            string_page += '<li><a data-itable-page="' + (start_page - 1) + '">..</a></li>';
        for (var i = start_page; i <= end_page; i++) {
            var active = i === currentpage;
            string_page += '<li' + (active ? ' class="active"' : '') + '><a' + (active ? '' : ' data-itable-page="' + i + '"') + '>' + (i + 1) + '</a></li>';
        }
        if (end_page + 1 < maxpage)
            string_page += '<li><a data-itable-page="' + (end_page + 1) + '">..</a></li>';
        string_page += '<li' + (currentpage + 1 === maxpage ? disable : '') + '><a' + (currentpage + 1 === maxpage ? '' : ' data-itable-page="' + (currentpage + 1) + '"') + '><span>&rsaquo;</span></a></li>';
        string_page += '<li' + (currentpage + 1 === maxpage ? disable : '') + '><a' + (currentpage + 1 === maxpage ? '' : ' data-itable-page="' + (maxpage - 1) + '"') + '><span>&raquo;</span></a></li>';
        pagination = string_page;
    }
    return {
        datainfo: datainfo,
        pagination: pagination
    };
}

function loading(open) {
    if (open) $('#div-loading').removeClass('hide');
    else $('#div-loading').addClass('hide');
}

function removedetail(t) {
    $(t).closest('tr').remove();
}